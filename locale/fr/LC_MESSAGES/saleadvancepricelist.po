# SOME DESCRIPTIVE TITLE.
# Copyright (C) Flectra S.A.
# This file is distributed under the same license as the flectra package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: flectra 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-12 14:46+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: ../../saleadvancepricelist/saleadvancepricelist.rst:5
msgid "Sale Advance Pricelist"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:9
msgid "Overview"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:11
msgid ""
"Sale Advance Pricelist module is developed to give an advance discount. "
"In this module, we have to give functionality to Add Price Rules, Cart "
"Rules, Coupon Rules and Discount Calculation Details Widget."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:15
msgid "Configuration"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:18
msgid "Install the Sale Advance Ecommerce Pricelist"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:20
msgid ""
"You’ll need to install the Sale Advance Ecommerce Pricelist module from "
"the app module in the Flectra backend. Go into the ``Apps`` and search "
"``Sale Advance Ecommerce Pricelist``."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:27
msgid "View Pricelists menu and Pricelist Items"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:29
msgid ""
"To enable the feature, go into the ``Settings`` ‣ ``USERS & COMPANIES`` ‣"
" ``Users``, select Users, tick ``Manage Pricelist Items`` and ``Sales "
"Pricelists`` and Save records."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:35
msgid "Pricelists Configuration"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:37
msgid "Go into the ``Sales`` ‣ ``CATALOG`` ‣ ``Pricelists`` select pricelist."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:43
msgid "Pricelist Type"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:45
msgid "We have introduced new fields like ``Pricelist Type``."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:47
msgid ""
"``Basic``: If you want to work with the default flow, then you can select"
" Basic. Default it is Basic."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:48
msgid ""
"``Advance``: If you want to use advanced features like Price Rule, Cart "
"Rules, and Coupon Codes then you can select Advanced."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:54
msgid "Apply Method"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:56
msgid "We have introduced new fields like ``Apply Method``."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:58
msgid ""
"``Apply First Matched Rules``: Calculate discount of the first match "
"conditions of Price Rules and Cart Rules."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:59
msgid ""
"``Apply All Matched Rules``: Calculate discount of the all match "
"conditions of Price Rules and Cart Rules."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:60
msgid ""
"``Apply Smallest Matched Discount``: Append minimum discount of every "
"match condition of Price Rules and Cart Rules and get minimum values from"
" the list of the Price Rules and Cart Rules."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:61
msgid ""
"``Apply Biggest Matched Discount``: Append maximum discount of every "
"match condition of Price Rules and Cart Rules and get maximum values from"
" the list of Price Rules and Cart Rules."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:64
msgid "Apply Coupon Code?"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:66
msgid "We have introduced new fields like ``Apply Coupon Code?``."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:68
msgid "Select apply coupon code option in pricelist for applying the coupon code."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:73
msgid "Price Rules"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:75
msgid ""
"If you selected ``Pricelist Type as Advanced`` then you can view price "
"rules' line in the pricelist. You can configuration price rules as per "
"your requirements."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:81
msgid ""
"If you want to add ``Price Rules`` then click on Add an item. Once you "
"click on Add an item you can see this window."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:89
#: ../../saleadvancepricelist/saleadvancepricelist.rst:149
msgid "Apply On"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:91
msgid "``Global``: Apply for all Product."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:92
msgid ""
"``Category``: Apply only for the selected category.If you select Apply on"
" as Category then new field Category will be visible."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:97
msgid ""
"``Product Template``: Apply only for selected Product Template "
"variants.If you select Apply on as Product Template then new field "
"Product Template will be visible."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:102
msgid ""
"``Product Variant``: Apply only for selected Product variants.If you "
"select Apply on as Product variants, then new field Product will be "
"visible."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:107
msgid ""
"If you want to add ``Price Rules Lines`` then click on Add an item. Once "
"you click on Add an item you can see this window."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:113
msgid "Rule Type"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:118
msgid ""
"``Fixed Amount``: If you want to give discounts in fixed amount then you "
"can select the rule type as Fixed Amount."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:117
msgid ""
"Fixed Amount converts into Percentage as per Unit Price of product in "
"sale order line. E.g. Product Unit Price is 885 and Discount Amount is 50"
" then 50 * 100 / 885 = 5.65"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:120
msgid ""
"``Percent``: If you want to give discounts in percentage then you can "
"select the rule type as Percent."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:123
msgid "Other Fields"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:125
msgid "``Discount Amount``: Define the amount of discount."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:127
msgid ""
"``Min Quantity``: Rule to apply if quantity must be greater than or equal"
" to the min quantity specified in this field."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:129
msgid ""
"``Max Quantity``: Rule to apply if quantity must be less than or equal to"
" the max quantity specified in this field."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:131
msgid ""
"``Start Date``: Rule to apply if start date must be greater than or equal"
" to the start date specified in this field."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:133
msgid ""
"``End Date``: Rule to apply if end date must be less than or equal to the"
" end date specified in this field."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:135
#: ../../saleadvancepricelist/saleadvancepricelist.rst:205
msgid ""
"``Condition``: If present, this condition must be satisfied before "
"executing the action rule."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:138
#: ../../saleadvancepricelist/saleadvancepricelist.rst:374
msgid "Cart Rules"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:143
msgid ""
"Cart Rules are applicable on sale order if the condition is matched which"
" defined in cart rules items."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:150
msgid ""
"``Subtotal At Least``: Cart Rules do not apply if Untaxed Amount is less "
"than define amount in cart line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:151
msgid ""
"``Subtotal less than``: Cart Rules apply if Untaxed Amount is less than "
"define amount in cart line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:152
msgid ""
"``Lines Count at least``: Cart Rules do not apply if line count of sale "
"order(duplicate product exclude) is less than define amount in cart line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:153
msgid ""
"``Lines less than``: Cart Rules apply if line count of sale "
"order(duplicate product exclude) is less than define amount in cart line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:154
msgid ""
"``Sum of Item Qty at least``: Cart Rules do not apply if the total "
"quantity of product in sale order is less than define amount in cart "
"line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:155
msgid ""
"``Sum of Item Qty less than``: Cart Rules apply if the total quantity of "
"product in sale order is less than define amount in cart line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:156
msgid ""
"``At least one product in order``: Cart Rules apply if product match in "
"sale order which defines in cart line. If you select Apply on as ``At "
"least one product in order`` then new field ``Product`` will be visible."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:161
msgid ""
"``None of selected Products``: Cart Rules do not apply if product match "
"in sale order which defines in cart line. If you select Apply on as "
"``None of selected Products`` then new field ``Products`` will be "
"visible."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:166
msgid ""
"``At least one category in order``: Cart Rules apply if product category "
"match in sale order which defines in cart line. If you select Apply on as"
" ``At least one category in order`` then new field ``Category`` will be "
"visible."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:171
msgid ""
"``None of selected Categories``: Cart Rules do not apply if product "
"category match in sale order which defines in cart line. If you select "
"Apply on as ``None of selected Categorie`` then new field ``Categories`` "
"will be visible."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:178
msgid "Coupon Rules"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:180
msgid ""
"Coupon Rules discounts for sale such as a fixed discount, a percentage "
"etc... of the entire sale order."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:183
msgid "Apply Coupon Code"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:184
msgid ""
"Select ``Apply Coupon Code?`` option in pricelist for applying the coupon"
" code."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:189
msgid ""
"Once you have selected Apply Coupon Code? An option you will display "
"Coupon Code line in pricelist."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:196
msgid "Coupon Type Basic Conditions"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:197
msgid "Coupon code rule to apply, if all conditions match which define below."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:199
msgid "``Total Usage Limit``: You can allow using maximum coupon limit."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:200
msgid ""
"``Remaining Usage Limit``: The limit is reduced when sale order will be "
"confirmed. If the limit is zero then this coupon code not used."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:201
msgid ""
"``Valid From``:- If Valid From must be greater than or equal to the Valid"
" From specified in this field."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:202
msgid ""
"``Valid To``: If Valid To must be less than or equal to the Valid To "
"specify in this field."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:203
msgid ""
"``Min Order Amount``: If Untaxed Amount of Sale order must be greater "
"than or equal to the Min Order Amount."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:204
msgid "``Apply On``: It is same as Price Rule(See :ref:`apply_on`)."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:210
msgid "Coupon Type"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:212
msgid "There are many Coupon Type which is listed below."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:217
msgid "Percent"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:219
msgid ""
"Add a percentage in the discount of every order line in which you apply "
"the coupon code."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:227
msgid "Fixed Amount"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:231
msgid ""
"If you want to give discounts on fixed amount then you can select Coupon "
"Type as Fixed Amount."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:231
#, python-format
msgid ""
"If the fixed amount is greater than or equal to the Unit Price of product"
" in sale order line, then it gives 100% discount.Fixed Amount convert "
"into Percentage as per Unit Price of product in sale order line. E.g. "
"Product Unit Price is 885 and Discount Amount is 50 then 50 * 100 / 885 ="
" 5.65"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:239
msgid "Buy X Product Get Y Product Free"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:241
msgid ""
"It applies in order line where product quantity is greater than or equal "
"to ``Number Of X Product`` in the coupon. Get ``Y same product`` unit "
"free when buying X product."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:249
msgid "Buy X Product Get Y Other Product Free"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:251
msgid ""
"It applies in order line where product quantity is greater than or equal "
"to ``Number Of X Product`` in the coupon. Get ``Number Of Y Product`` "
"Other Product(``[C-Case] Computer Case``) when buying X product."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:259
msgid "Range Based Discount(Buy X Product Get Percent Free)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:261
msgid ""
"It applies in order line where product quantity is greater than or equal "
"to ``Number Of X Product`` in the coupon code and get percent "
"free(``7.75``) when buying X product."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:269
msgid "Clubbed Discount"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:271
msgid "Add a Discount and Extra Discount on Sale order line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:276
msgid ""
"You can view sale order in the particular coupon code. Go into "
"``Pricelist`` ‣ ``Coupon Code``‣  open Coupon Code."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:283
msgid "Sale Orders"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:285
msgid ""
"We have to give functionality to add price rules discount, cart rules "
"discount, coupon rules discount and view discount calculation details "
"widget."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:291
msgid "The discount is automatically changed when order line updates."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:294
#: ../../saleadvancepricelist/saleadvancepricelist.rst:402
#: ../../saleadvancepricelist/saleadvancepricelist.rst:438
#: ../../saleadvancepricelist/saleadvancepricelist.rst:510
#: ../../saleadvancepricelist/saleadvancepricelist.rst:549
#: ../../saleadvancepricelist/saleadvancepricelist.rst:593
#: ../../saleadvancepricelist/saleadvancepricelist.rst:629
msgid "First order line discount calculation"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:296
msgid "``Product Rule Line``:"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:298
msgid ""
"First order line quantity 5 is between 1 to 5 in Product Rule Line(See "
":ref:`price_rule`). Here define **Discount Amount = 50** and **Rule Type "
"= Fixed Amount** so we need to convert into a percentage."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:304
#: ../../saleadvancepricelist/saleadvancepricelist.rst:324
#: ../../saleadvancepricelist/saleadvancepricelist.rst:460
#: ../../saleadvancepricelist/saleadvancepricelist.rst:491
msgid ""
"Percentage Calculation Formule = Discount Amount * 100 / Unit Price of "
"Product"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:305
msgid "50 * 100 / 885 = 5.65"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:307
#: ../../saleadvancepricelist/saleadvancepricelist.rst:327
msgid "``Cart Rules``:-"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:309
#: ../../saleadvancepricelist/saleadvancepricelist.rst:329
#, python-format
msgid ""
"6.5 % of Match first Cart Rules because **Subtotal(Untaxed "
"Amount=12,012.55)** At least: 2,500.00."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:311
msgid "Discount = Product Rule line(**5.65**) + Cart Rules(**6.5**)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:313
msgid "``So, discount is 5.65 + 6.5 = 12.15%``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:316
#: ../../saleadvancepricelist/saleadvancepricelist.rst:468
msgid "Second order line discount calculation"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:318
msgid ""
"Second order line quantity 3 is between 1 to 5 in Product Rule Line(See "
":ref:`price_rule`). Here define **Discount Amount = 50** and **Rule Type "
"= Fixed Amount** so we need to convert into a percentage."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:325
msgid "50 * 100 / 2950 = 1.69"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:331
msgid "Discount = Product Rule line(**1.69**) + Cart Rules(**6.5**)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:333
msgid "``So, discount is 1.69 + 6.5 = 8.19%``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:336
msgid "Third order line discount calculation"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:338
msgid ""
"Third order line quantity 3 is between 1 to 5 in Product Rule Line(See "
":ref:`price_rule`)."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:340
msgid "Unit Price(25) is less than Fixed Amount(50) of Product Rule Lines."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:342
#, python-format
msgid "``So, it gives 100% discount for that line``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:346
msgid "Discount Calculation Informations"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:355
msgid ""
"Once you click on information icon button, you can view discount "
"calculation details. Here display total Price Rule, Cart Rules, and "
"Coupon Code discount etc.."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:359
msgid "Sale Orders With Coupon Code"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:361
msgid "Coupon code price rule lines and cart rules:"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:366
msgid "Price Rule Lines"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:379
msgid "We have already configured Coupon code(See :ref:`coupon_type`)."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:382
msgid "Coupon Code Warning"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:387
msgid ""
"We have entered coupon code ``Get10Peroff`` and click on ``Apply Coupon "
"Code`` button it will give the warning message if subtotal is less than "
"min order amount in the coupon code. Here in the coupon code Min Order "
"Amount set 2500.00 and Subtotal is 1639.02 (See :ref:`coupon_percent`)."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:391
msgid "Sale Orders With Percentage Coupon Code"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:396
msgid ""
"In this Sale order, we have entered coupon code ``Get10Peroff`` and click"
" on ``Apply Coupon Code`` button. After the click on ``Apply Coupon "
"Code`` discount value changed in sale order line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:404
#: ../../saleadvancepricelist/saleadvancepricelist.rst:440
#: ../../saleadvancepricelist/saleadvancepricelist.rst:470
#: ../../saleadvancepricelist/saleadvancepricelist.rst:512
#: ../../saleadvancepricelist/saleadvancepricelist.rst:551
#: ../../saleadvancepricelist/saleadvancepricelist.rst:595
#: ../../saleadvancepricelist/saleadvancepricelist.rst:631
msgid "``Product Lines Rules``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:406
#: ../../saleadvancepricelist/saleadvancepricelist.rst:514
#: ../../saleadvancepricelist/saleadvancepricelist.rst:553
#: ../../saleadvancepricelist/saleadvancepricelist.rst:597
#: ../../saleadvancepricelist/saleadvancepricelist.rst:633
#, python-format
msgid ""
"4.9 % of Match first Product Rule Lines (See :ref:`price_rule_line`) "
"because ordered Qty 5 is between 1 to 6."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:408
#: ../../saleadvancepricelist/saleadvancepricelist.rst:444
#: ../../saleadvancepricelist/saleadvancepricelist.rst:474
#: ../../saleadvancepricelist/saleadvancepricelist.rst:516
#: ../../saleadvancepricelist/saleadvancepricelist.rst:555
#: ../../saleadvancepricelist/saleadvancepricelist.rst:599
#: ../../saleadvancepricelist/saleadvancepricelist.rst:635
msgid "``Cart Rules``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:410
#: ../../saleadvancepricelist/saleadvancepricelist.rst:518
#: ../../saleadvancepricelist/saleadvancepricelist.rst:557
#: ../../saleadvancepricelist/saleadvancepricelist.rst:601
#: ../../saleadvancepricelist/saleadvancepricelist.rst:637
msgid ""
"First cart Rules (See :ref:`cart_rule_line`) don't match because Subtotal"
" is greater than 3,000.00 so that rules skip."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:412
msgid ""
"Second Cart Rules (See :ref:`cart_rule_line`) match because of Sum of "
"Item Qty at least: 7.00 (here 8). So add 10 %."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:415
#: ../../saleadvancepricelist/saleadvancepricelist.rst:451
#: ../../saleadvancepricelist/saleadvancepricelist.rst:481
#: ../../saleadvancepricelist/saleadvancepricelist.rst:523
#: ../../saleadvancepricelist/saleadvancepricelist.rst:562
#: ../../saleadvancepricelist/saleadvancepricelist.rst:606
#: ../../saleadvancepricelist/saleadvancepricelist.rst:642
msgid "Quantity : First order line quantity (5) + Second order line quantity (3)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:417
msgid ""
"``Coupon Code`` : Coupon code (See :ref:`coupon_percent`) get 10 percent "
"free."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:419
msgid ""
"Discount = Product Rule line(**4.9**) + Cart Rules(**10**) + Coupon "
"Code(**10**)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:421
msgid "``So, First line discount is 24.9 %( 4.9 + 10 + 10)``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:424
#: ../../saleadvancepricelist/saleadvancepricelist.rst:534
#: ../../saleadvancepricelist/saleadvancepricelist.rst:615
msgid "Same calculation for the second line of sale order."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:427
msgid "Sale Orders With Fixed Amount Coupon Code"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:432
msgid ""
"In this Sale order, we have entered coupon code ``Get20off`` and click on"
" ``Apply Coupon Code`` button. After the click on ``Apply Coupon Code`` "
"discount value changed in sale order line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:442
#: ../../saleadvancepricelist/saleadvancepricelist.rst:472
#, python-format
msgid ""
"4.9 % of match first Product Rule Lines (See :ref:`price_rule_line`) "
"because ordered Qty 5 is between 1 to 6."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:446
#: ../../saleadvancepricelist/saleadvancepricelist.rst:476
msgid ""
"First Cart Rules (See :ref:`cart_rule_line`) don't match because Subtotal"
" is greater than 3,000.00 so that rules skip."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:448
#: ../../saleadvancepricelist/saleadvancepricelist.rst:478
#: ../../saleadvancepricelist/saleadvancepricelist.rst:520
#: ../../saleadvancepricelist/saleadvancepricelist.rst:559
#: ../../saleadvancepricelist/saleadvancepricelist.rst:603
#: ../../saleadvancepricelist/saleadvancepricelist.rst:639
msgid ""
"Second Cart Rules (See :ref:`cart_rule_line`) match because of Sum of "
"Item Qty at least: 7.00 (here 8). so add 10 %."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:453
#: ../../saleadvancepricelist/saleadvancepricelist.rst:484
msgid ""
"``Coupon Code`` : Coupon code (See :ref:`coupon_fixed_amount`) get fixed "
"amount 20 per product."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:455
#: ../../saleadvancepricelist/saleadvancepricelist.rst:486
msgid ""
"Here define **Discount Amount = 20** so we need to convert into the "
"percentage."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:461
msgid "20 * 100 / 885 = 2.26"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:463
msgid ""
"Discount = Product Rule line(**4.9**) + Cart Rules(**10**) + Coupon "
"Code(**2.26**)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:465
msgid "``So, First line discount is 17.6 %( 4.9 + 10 + 2.26)``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:492
msgid "20 * 100 / 2950 = 0.68"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:494
msgid ""
"Discount = Product Rule line(**4.9**) + Cart Rules(**10**) + Coupon "
"Code(**0.68**)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:496
msgid "``So, First line discount is 15.58 %( 4.9 + 10 + 0.68)``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:499
msgid "Sale Orders With Buy X Product Get Y Product Free Coupon Code"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:504
msgid ""
"In this Sale order, we have entered coupon code ``BXGYFree`` and click on"
" ``Apply Coupon Code`` button. After the click on ``Apply Coupon Code`` 2"
" more line created in sale order line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:525
msgid ""
"``Coupon Code`` : Coupon code (See :ref:`coupon_buy_x_get_y`) get y "
"number of product free."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:527
msgid ""
"In this coupon code we get 1 same product free of buy every 3 qty. ``so, "
"int(5/3) = 1``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:529
#: ../../saleadvancepricelist/saleadvancepricelist.rst:577
msgid "Discount = Product Rule line(**4.9**) + Cart Rules(**10**)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:531
msgid ""
"``So, First line discount is 14.9 %( 4.9 + 10) and get free product line "
"3 extra``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:538
msgid "Sale Orders With Buy X Product Get Y Product Other Free Coupon Code"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:543
msgid ""
"In this Sale order, we have entered coupon code ``BXGYOtherFree`` and "
"click on ``Apply Coupon Code`` button. After the click on ``Apply Coupon "
"Code`` 1 more line created with product ``[C-Case] Computer Case`` in "
"sale order line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:565
msgid ""
"``Coupon Code`` : Coupon code (See :ref:`coupon_buy_x_get_y_other`) get y"
" number of other product free."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:567
msgid ""
"In this coupon code, we get 3 ``[C-Case] Computer Case`` product free. "
"**Calculation of third order line** buy every 2 quantity get 1 product "
"free."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:570
msgid ""
"``Quantity Calculation:`` **int(Order Line Quantity / Number of X "
"Product)**"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:572
msgid "first order line quantity(5):- int(5/2) = 2"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:573
msgid "second order line quantity(3):-int(3/2) = 1"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:575
msgid "``So, the total quantity of third order line is 3.``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:579
msgid ""
"``So, First line discount is 14.9 %( 4.9 + 10) and get free "
"product([C-Case] Computer Case) with 3 Quantity``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:582
msgid "Sale Orders With Clubbed Discount Coupon Code"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:587
msgid ""
"In this Sale order, we have entered coupon code ``CD15Per`` and click on "
"``Apply Coupon Code`` button. After the click on ``Apply Coupon Code`` "
"discount value changed in sale order line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:608
msgid ""
"``Coupon Code`` : Coupon code (See :ref:`coupon_clubbed`) get 15 percent "
"free."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:610
msgid ""
"Discount = Product Rule line(**4.9**) + Cart Rules(**10**) + Coupon "
"Code(**15**)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:612
msgid "``So, First line discount is 29.9 %( 4.9 + 10 + 15)``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:618
msgid "Sale Orders With Range Based Coupon Code"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:623
msgid ""
"In this Sale order, we have entered coupon code ``BXGPercentFree`` and "
"click on ``Apply Coupon Code`` button. After the click on ``Apply Coupon "
"Code`` discount value changed in sale order line."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:644
msgid ""
"``Coupon Code`` : Coupon code (See :ref:`coupon_range_based`) we get 7.75"
" percent free of buy every 3 qty."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:646
msgid "In this coupon code, we get 7.75 percent free of buy every 3 qty."
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:648
msgid ""
"Discount = Product Rule line(**4.9**) + Cart Rules(**10**) + Coupon "
"Code(**7.75**)"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:650
msgid "``So, First line discount is 22.65 %( 4.9 + 10 + 7.75)``"
msgstr ""

#: ../../saleadvancepricelist/saleadvancepricelist.rst:653
msgid ""
"Coupon code does not apply to the second line of sale order because "
"Ordered Qty is less than 3."
msgstr ""

